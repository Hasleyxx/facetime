import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonService } from '../services/common.service';
import { EmployeeService } from '../services/employee.service';
import { logs } from '../interfaces';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { LogService } from '../services/log.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ViewLogComponent } from '../view-log/view-log.component';

@Component({
  selector: 'app-employee-logs',
  templateUrl: './employee-logs.component.html',
  styleUrls: ['./employee-logs.component.css']
})
export class EmployeeLogsComponent implements OnInit {

  bio_id: number;
  loading: boolean = true;
  logs = [];
  name: string;
  dataSource: MatTableDataSource<logs>;
  displayedColumns = ["no", "timeStatus", "date_created", "action"];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private common: CommonService,
    private empService: EmployeeService,
    private logService: LogService,
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.common.checkLogin();
    this.bio_id = parseInt(this.route.snapshot.paramMap.get("bio_id"));
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.loading = true;
    this.logs = [];

    this.empService.getOneEmployee(this.bio_id).subscribe(data => {
      if (data == null) {
        this.name = "Undefined";
      } else {
        this.name = data.name;
      }
    });

    this.logService.getLogs(this.bio_id).subscribe(data => {
      data.forEach((element, index) => {
        this.logs.push(element);

        if (index + 1 == data.length) {
          this.loading = false;
          this.dataSource = new MatTableDataSource(this.logs);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      });
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  viewLog(id: number) {
    const dialogRef = this.dialog.open(ViewLogComponent, {
      data: {
        id: id
      },
      width: '70%'
    });
  }

  back() {
    this.router.navigateByUrl("/employees");
  }

  reload() {
    this.getData();
  }
}
