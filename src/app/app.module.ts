import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Global } from './global';

import { LayoutModule } from '@angular/cdk/layout';
import { MaterialModule } from './material-module';
import { LoginComponent } from './auth/login/login.component';
import { EmployeesComponent } from './employees/employees.component';
import { CommonService } from './services/common.service';
import { ConfirmDialogComponent } from './element/confirm-dialog/confirm-dialog.component';
import { NewEmployeeComponent } from './new-employee/new-employee.component';
import { UpdateEmployeeComponent } from './update-employee/update-employee.component';
import { EmployeeLogsComponent } from './employee-logs/employee-logs.component';
import { ViewLogComponent } from './view-log/view-log.component';
import { ArchiveEmployeesComponent } from './archive-employees/archive-employees.component';

@NgModule({
  declarations: [AppComponent, LoginComponent, EmployeesComponent, ConfirmDialogComponent, NewEmployeeComponent, UpdateEmployeeComponent, EmployeeLogsComponent, ViewLogComponent, ArchiveEmployeesComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    LayoutModule,
    MaterialModule
  ],
  providers: [Global, CommonService],
  entryComponents: [
    ConfirmDialogComponent,
    NewEmployeeComponent,
    UpdateEmployeeComponent,
    ViewLogComponent,
    ArchiveEmployeesComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
