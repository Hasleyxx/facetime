import { Injectable } from '@angular/core';
import { Global } from '../global';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { logs } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(
    private global: Global,
    private http: HttpClient
  ) { }

  getLogs(bio_id: number): Observable<logs[]> {
    return this.http.get<logs[]>(this.global.url + "/getLogs/" + bio_id);
  }

  getOneLog(id: number): Observable<logs> {
    return this.http.get<logs>(this.global.url + "/getOneLog/" + id);
  }
}
