import { Injectable } from '@angular/core';
import { Global } from '../global';
import { HttpClient } from '@angular/common/http';
import { users } from '../interfaces';
import { Observable } from 'rxjs';
import { Md5 } from 'md5-typescript';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private global: Global,
    private http: HttpClient
  ) { }

  login(username: string): Observable<users> {
    return this.http.get<users>(this.global.url + "/getUser/" + username);
  }
}
