import { Injectable } from '@angular/core';
import { Global } from '../global';
import { HttpClient } from '@angular/common/http';
import { employees, addEmployee } from '../interfaces';
import { Observable } from 'rxjs';

export interface deleteEmp {
  id: number,
  date_deleted: string
}

@Injectable({
  providedIn: "root"
})
export class EmployeeService {
  constructor(private global: Global, private http: HttpClient) {}

  getEmployees(): Observable<employees[]> {
    return this.http.get<employees[]>(this.global.url + "/getEmployees");
  }

  getEmployeesArchive(): Observable<employees[]> {
    return this.http.get<employees[]>(this.global.url + "/getEmployeesArchive");
  }

  getOneEmployee(bio_id: number): Observable<employees> {
    return this.http.get<employees>(
      this.global.url + "/getOneEmployee/" + bio_id
    );
  }

  getEmployeeById(id: number): Observable<employees> {
    return this.http.get<employees>(
      this.global.url + "/getEmployeeById/" + id
    );
  }

  updateEmployee(data: employees): Observable<employees> {
    return this.http.post<employees>(
      this.global.url + "/updateEmployee/", data
    );
  }

  addEmployee(data: addEmployee): Observable<addEmployee> {
    return this.http.post<addEmployee>(this.global.url + "/addEmployee", data);
  }

  deleteEmp(data: deleteEmp): Observable<number> {
    return this.http.post<number>(this.global.url + "/deleteEmp", data);
  }
}
