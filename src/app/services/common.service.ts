import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { MatSnackBar } from '@angular/material';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: "root"
})
export class CommonService {
  public dateTime: any;

  constructor(private router: Router, private _snackBar: MatSnackBar) {
    this.dateTime = moment().format("YYYY-MM-DD h:mm:ss");
  }

  protected loginStatus = new Subject<any>();
  loginStatusChange = this.loginStatus.asObservable();

  public checkLogin() {
    let username = sessionStorage.getItem("username");
    let bool: boolean = false;
    if (username !== null) {
      bool = true;
    }

    this.loginStatus.next(bool);
  }

  openSnackbar(content: string) {
    this._snackBar.open(content, "close", {
      duration: 3000
    });
  }
}
