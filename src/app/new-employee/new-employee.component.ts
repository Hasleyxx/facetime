import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ConfirmDialogComponent } from '../element/confirm-dialog/confirm-dialog.component';
import { CommonService } from '../services/common.service';

@Component({
  selector: "app-new-employee",
  templateUrl: "./new-employee.component.html",
  styleUrls: ["./new-employee.component.css"]
})
export class NewEmployeeComponent implements OnInit {

  public id: number;
  public empForm: FormGroup;
  public depts = [
    { value: "Operation" },
    { value: "Laboratory" },
    { value: "IT Department" },
    { value: "Accounting Department" }
  ];
  public defaultDept: any;

  constructor(
    private common: CommonService,
    private empService: EmployeeService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<NewEmployeeComponent>
  ) {
    this.defaultDept = this.depts[0]['value'];
  }

  ngOnInit() {
    this.empForm = this.formBuilder.group({
      bio_id: ["", Validators.required],
      name: ["", Validators.required],
      dept: ["", Validators.required]
    });
  }

  empFormSubmit() {
    let name = this.empForm.get("name").value;
    let bio_id = this.empForm.get("bio_id").value;

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "Are You Sure?",
        content: "`" + name + "` will be added to Employee Lists"
      },
      width: "25%"
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        this.empService.getOneEmployee(bio_id).subscribe(data => {
          if (data == null) {
            this.empService.addEmployee(this.empForm.value).subscribe((data: any) => {
              if (data == 1) {
                this.dialogRef.close("added");
              } else {
                this.common.openSnackbar("Failed to add new employee.");
              }
            });
          } else {
            this.common.openSnackbar("Biometric ID is already registered. Choose another one");
          }
        });
      }
    });
  }
}
