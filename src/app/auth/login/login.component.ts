import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Md5 } from 'md5-typescript';
import { CommonService } from 'src/app/services/common.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private common: CommonService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  loginFormSubmit() {
    let password = Md5.init(this.loginForm.get('password').value);
    let username = this.loginForm.get('username').value;

    this.userService.login(username).subscribe(data => {
      if (data == null) {
        this.common.openSnackbar("Account not found.");
      } else {
        let checkPassword = data.password;

        if (password !== checkPassword) {
          this.common.openSnackbar("Password is incorrect.");
        } else {
          sessionStorage.setItem("username", username);
          this.router.navigateByUrl('/employees');
        }
      }
    });
  }

}
