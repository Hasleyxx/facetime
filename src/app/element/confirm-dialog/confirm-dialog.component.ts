import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { confirmDialog } from 'src/app/interfaces';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css']
})
export class ConfirmDialogComponent implements OnInit {

  public title: string = "";
  public content: string = "";

  constructor(
    @Inject(MAT_DIALOG_DATA) private dialogData: confirmDialog,
    private dialogRef: MatDialogRef<ConfirmDialogComponent>
  ) {
    this.title = this.dialogData.title;
    this.content = this.dialogData.content;
  }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close("ok");
  }

}
