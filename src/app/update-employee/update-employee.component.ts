import { Component, OnInit, Inject } from "@angular/core";
import { EmployeeService } from "../services/employee.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { ConfirmDialogComponent } from "../element/confirm-dialog/confirm-dialog.component";
import { CommonService } from "../services/common.service";

@Component({
  selector: "app-update-employee",
  templateUrl: "./update-employee.component.html",
  styleUrls: ["./update-employee.component.css"]
})
export class UpdateEmployeeComponent implements OnInit {
  public bio_id: number;
  public empForm: FormGroup;
  public depts = [
    { value: "Operation" },
    { value: "Laboratory" },
    { value: "IT Department" },
    { value: "Accounting Department" }
  ];
  public defaultDept: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private common: CommonService,
    private empService: EmployeeService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<UpdateEmployeeComponent>
  ) {
    this.bio_id = this.dialogData.bio_id;
  }

  ngOnInit() {
    this.empForm = this.formBuilder.group({
      id: [""],
      bio_id: [""],
      name: ["", Validators.required],
      dept: ["", Validators.required]
    });

    this.getData();
  }

  getData() {
    this.empService.getOneEmployee(this.bio_id).subscribe(data => {
      if (data !== null) {
        this.empForm.patchValue({
          id: data.id,
          bio_id: data.bio_id,
          name: data.name,
          dept: data.dept
        });
        this.defaultDept = data.dept;
      }
    });
  }

  empFormSubmit() {
    let id = this.empForm.get("id").value;
    let bio_id = this.empForm.get("bio_id").value;
    let name = this.empForm.get("name").value;
    let dept = this.empForm.get("dept").value;

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "Are You Sure?",
        content: "`" + name + "'s` data will be updated!"
      },
      width: "25%"
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        this.empService.getEmployeeById(id).subscribe(data => {
          if (data == null) {
            this.common.openSnackbar("Employee data not found! Please contact IT for assistance.");
          } else {
            this.empService.updateEmployee(this.empForm.value).subscribe((data: any) => {
              if (data == 1) {
                this.dialogRef.close("updated");
                this.common.openSnackbar("Employee data has been updated!");
              } else {
                this.common.openSnackbar("Failed to update employee data. Please contact IT for assistance.");
              }
            })
          }
        });
      }
    });
  }
}
