import { Component, OnInit, Inject } from '@angular/core';
import { LogService } from '../services/log.service';
import { MAT_DIALOG_DATA } from '@angular/material';
import { logs } from '../interfaces';

export interface dialogData {
  id: number
}

@Component({
  selector: 'app-view-log',
  templateUrl: './view-log.component.html',
  styleUrls: ['./view-log.component.css']
})
export class ViewLogComponent implements OnInit {

  public id: number;
  public log: logs;
  public name: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData: dialogData,
    private logService: LogService
  ) {
    this.id = this.dialogData.id;
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.logService.getOneLog(this.id).subscribe(data => {
      this.log = data;
      this.name = data.name;
    });
  }

}
