import { Component, ChangeDetectorRef } from "@angular/core";
import { MediaMatcher } from '@angular/cdk/layout';
import { Router } from '@angular/router';
import { CommonService } from './services/common.service';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  snavBool: boolean = false;
  loginStatus: boolean = false;
  title = "Biometric Face Stamp Client";
  navs = [
    {
      label: "Employees",
      route: "/employees",
      icon: "account_box"
    }
  ];

  private _mobileQueryListener: () => void;
  mobileQuery: MediaQueryList;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private router: Router,
    private common: CommonService
  ) {
    this.mobileQuery = media.matchMedia("(max-width: 600px)");
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this.common.loginStatusChange.subscribe(data => {
      this.loginStatus = data;
    });
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  logout() {
    sessionStorage.removeItem("username");
    this.common.checkLogin();
    this.router.navigateByUrl("/login");
  }
}
