export interface confirmDialog {
  title: string,
  content: string
}

export interface users {
  id: number,
  username: string,
  password: string
}

export interface employees {
  id: number,
  name: string,
  bio_id: number,
  dept: string,
  inOutStatus: string,
  status: number,
  date_created: string
}

export interface addEmployee {
  name: string,
  bio_id: number,
  dept: string
}

export interface logs {
  id: number,
  bio_id: number,
  name: string,
  image: string,
  timeStatus: string,
  date_created: string
}
