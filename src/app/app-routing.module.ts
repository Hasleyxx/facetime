import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { EmployeesComponent } from './employees/employees.component';
import { AuthGuard } from './auth-guard';
import { EmployeeLogsComponent } from './employee-logs/employee-logs.component';

const routes: Routes = [
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "employees",
    component: EmployeesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "employees/:bio_id",
    component: EmployeeLogsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "**",
    redirectTo: "login"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
