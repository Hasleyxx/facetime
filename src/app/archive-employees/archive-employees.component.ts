import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonService } from '../services/common.service';
import { EmployeeService } from '../services/employee.service';
import { employees } from '../interfaces';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-archive-employees',
  templateUrl: './archive-employees.component.html',
  styleUrls: ['./archive-employees.component.css']
})
export class ArchiveEmployeesComponent implements OnInit {

  loading: boolean = true;
  employees = [];
  dataSource: MatTableDataSource<employees>;
  displayedColumns = ["no", "bio_id", "name", "dept", "date_deleted"];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private common: CommonService,
    private empService: EmployeeService,
    private route: Router
  ) {
    this.common.checkLogin();
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.loading = true;
    this.employees = [];
    this.empService.getEmployeesArchive().subscribe(data => {
      data.forEach((element, index) => {
        this.employees.push(element);

        if (index + 1 == data.length) {
          this.loading = false;
          this.dataSource = new MatTableDataSource(this.employees);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      });
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  back() {
    this.route.navigateByUrl("/employees");
  }

}
