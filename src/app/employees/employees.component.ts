import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CommonService } from '../services/common.service';
import { EmployeeService } from '../services/employee.service';
import { employees } from '../interfaces';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../element/confirm-dialog/confirm-dialog.component';
import { NewEmployeeComponent } from '../new-employee/new-employee.component';
import { UpdateEmployeeComponent } from '../update-employee/update-employee.component';
import { Router } from '@angular/router';
import { ArchiveEmployeesComponent } from '../archive-employees/archive-employees.component';
import * as moment from 'moment';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: "app-employees",
  templateUrl: "./employees.component.html",
  styleUrls: ["./employees.component.css"]
})
export class EmployeesComponent implements OnInit {
  default: any;
  loading: boolean = true;
  employees = [];
  dataSource: MatTableDataSource<employees>;
  displayedColumns = ["no", "bio_id", "name", "dept", "action"];
  form: FormGroup;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private common: CommonService,
    private empService: EmployeeService,
    private dialog: MatDialog,
    private route: Router,
    private formBuilder: FormBuilder
  ) {
    this.common.checkLogin();
  }

  ngOnInit() {
    this.getData();

    this.form = this.formBuilder.group({
      option: ['']
    });
  }

  getData() {
    this.loading = true;
    this.employees = [];
    this.empService.getEmployees().subscribe(data => {
      data.forEach((element, index) => {
        this.employees.push(element);

        if (index + 1 == data.length) {
          this.loading = false;
          this.dataSource = new MatTableDataSource(this.employees);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      });
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selectOptions(value) {
    if (value == "newEmp") {
      this.addEmployeeDialog();
    } else {
      this.viewArchivedEmployeeDialog();
    }

    setTimeout(() => {
      this.default = "-";
      this.form.patchValue({
        option: "-"
      });
    }, 1000);
  }

  addEmployeeDialog() {
    const dialogRef = this.dialog.open(NewEmployeeComponent, {
      width: '40%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "added") {
        this.getData();
        this.common.openSnackbar("New Employee has been added");
      }
    });
  }

  viewArchivedEmployeeDialog() {
    const dialogRef = this.dialog.open(ArchiveEmployeesComponent, {
      width: '90%',
      autoFocus: false
    });
  }

  viewLogs(bio_id: number) {
    this.route.navigateByUrl("/employees/" + bio_id);
  }

  updateEmployee(bio_id: number) {
    const dialogRef = this.dialog.open(UpdateEmployeeComponent, {
      data: {
        bio_id: bio_id
      },
      width: '40%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "updated") {
        this.getData();
        this.common.openSnackbar("Employee data has been updated");
      }
    });
  }

  deleteEmployee(id: number) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "Caution!",
        content: "Employee will be moved to archived!"
      },
      width: "25%"
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "ok") {
        let data = {
          id: id,
          date_deleted: moment().format("YYYY-MM-DD h:mm:ss")
        };
        this.empService.deleteEmp(data).subscribe(data => {
          if (data == 1) {
            this.common.openSnackbar(
              "Employee #" + id + " has been moved to archived!"
            );
            this.getData();
          } else {
            this.common.openSnackbar("Failed to move employee to archive!");
          }
        });
      }
    });
  }
}
